import gitlab
import os
import fnmatch

#connecting and authenticating to gitlab
gl = gitlab.Gitlab('https://gitlab.com', private_token=os.environ["PRIVATE_TOKEN"])
gl.auth()

env_variables=['NAMESPACE', 'PROJECT', 'RELEASE']


#validating the input parameters
#if os.environ['NAMESPACE'].strip() and os.environ['PROJECT'].strip() and os.environ['RELEASE'].strip():
if set(env_variables).issubset(os.environ):
    namespace_with_path = os.environ['NAMESPACE']+'/'+os.environ['PROJECT']
    release = os.environ['RELEASE']

    #fetching the project    
    try:
        my_project = gl.projects.get(namespace_with_path)
    except gitlab.exceptions.GitlabError:
        print('Please provide valid namespace and project name')
        exit()
else:
    print('Please provide Namespace, Project and Release fields')
    exit()
    
deleted_hooks = []

for hook in my_project.hooks.list():
#validating and deleting the webhook
    print('-----------')
    print(hook.url.split('/')[-1].strip().upper())
    print(release.upper())
    if fnmatch.fnmatch(hook.url.split('/')[-1].strip().upper(), '*'+release.upper()+'*'):
        my_project.hooks.delete(hook.id)
        deleted_hooks.append(hook.url)

if deleted_hooks:
    print('deleted webhooks:')
    print("\n".join(str(x) for x in deleted_hooks))
else:
    print('Please provide valid release name currently only supporting for "SITE_xx.*" format or your release name is not present in specified repository')