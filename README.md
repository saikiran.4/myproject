# myproject

#installing python3 into centos without yum
wget https://www.python.org/ftp/python/3.7.4/Python-3.7.4.tar.xz
tar xf Python-3.*
cd Python-3.*
./configure
make
make install

installing python-gitlab with pip3
pip3 install --upgrade python-gitlab
clone https://github.com/python-gitlab/python-gitlab
cd python-gitlab
python setup.py install
